const http = require('http');
const fs = require('fs');

const { v4: uuidv4 } = require('uuid');

const port= require('./config.js');

const server = http.createServer((req, res)=>{
    if(req.url === '/html')
    {
      res.setHeader('Content-type','text/html')

        res.write(`<!DOCTYPE html>
        <html>
          <head>
          </head>
          <body>
              <h1>Any fool can write code that a computer can understand. Good programmers write code that humans can understand.</h1>
              <p> - Martin Fowler</p>
        
          </body>
        </html>`)
        res.end()
    }else if(req.url === '/json')
    {

      res.setHeader('Content-type','application/json')

        res.write(`{
            "slideshow": {
              "author": "Yours Truly",
              "date": "date of publication",
              "slides": [
                {
                  "title": "Wake up to WonderWidgets!",
                  "type": "all"
                },
                {
                  "items": [
                    "Why <em>WonderWidgets</em> are great",
                    "Who <em>buys</em> WonderWidgets"
                  ],
                  "title": "Overview",
                  "type": "all"
                }
              ],
              "title": "Sample Slide Show"
            }
          }`)
          res.end();
    }else if(req.url === '/uuid')
    {
      res.setHeader('Content-type','application/json')

          res.end(JSON.stringify({
            uuid: uuidv4(),
          }));
    }
    else if(req.method==="GET" && req.url.startsWith('/status'))
    {
      
        const statusCodeArr = req.url.split('/')
        const statusCode = req.url.split('/')[2];
        
          if(statusCode in http.STATUS_CODES && statusCodeArr.length === 3)
          {
            res.statusCode= statusCode;
             res.end(JSON.stringify({'status code' : statusCode}))
          }else
          {
             res.statusCode= 400;
             res.end(JSON.stringify({'status code' : 'Invalid'}))
          }
  
    }else if(req.method==="GET" && req.url.startsWith('/delay'))
    {
       const timeArr = req.url.split('/');

        const time = +req.url.split('/').pop()

          if(time< 0)
          {
            res.statusCode= 400;
            res.end(JSON.stringify({'Response' : 'negative number'}))
          }
          else if(time >=0 && timeArr.length === 3)
          {
              setTimeout(() => {
              res.statusCode = 200;
              res.end(JSON.stringify({'Response after' : time}))
              },time*1000);
          }else
          {
            res.statusCode= 400;
            res.end(JSON.stringify({'Response' : 'Invalid'}))
          }

    }else
    {
      res.setHeader('Content-type','text/html')
      
      res.statusCode= 400;
      res.end(`<h1>Main Page<h1>`)
    }

})

server.listen(port, ()=>{
    console.log(`${port} port is running`);
})